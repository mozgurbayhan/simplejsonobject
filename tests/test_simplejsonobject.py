# -*- coding: utf-8 -*-
import os
import sys
import unittest

import pytest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from simplejsonobject import JsonObject

__author__ = 'ozgur'
__creation_date__ = '2.12.2019' '13:07'


class Sample(JsonObject):
    def __init__(self, a):
        self.a = a
        self.b = 3
        self.c = None


class Sample2(JsonObject):
    def __init__(self):
        self.a = 9
        self.b = 3
        self.c = None


class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_01_out_dict(self):
        tdata = "8"
        smp = Sample(tdata)
        self.assertEqual(smp.to_dict(), {"a": tdata, "b": 3, "c": None})
        self.assertEqual(smp.to_dict_compressed(), {"a": tdata, "b": 3})

    def test_02_out_json(self):
        smp = Sample(8)
        self.assertEqual(smp.to_json(), '{\n    "a": 8,\n    "b": 3,\n    "c": null\n}')
        self.assertEqual(smp.to_json_compressed(), '{"a": 8, "b": 3}')

    def test_03_in(self):
        tdata = "8"
        smp = Sample(tdata)

        tsmp = Sample("12")
        tsmp.from_dict({"a": tdata, "b": 3, "c": None})
        self.assertEqual(tsmp.__dict__, smp.__dict__)

        tsmp = Sample("12")
        tsmp.from_dict({"a": tdata, "b": 3})
        self.assertEqual(tsmp.__dict__, smp.__dict__)

        tsmp = Sample("12")
        tsmp.from_json('{\n    "a": "8",\n    "b": 3,\n    "c": null\n}')
        self.assertEqual(tsmp.__dict__, smp.__dict__)

        tsmp = Sample("12")
        tsmp.from_json('{"a": "8", "b": 3}')
        self.assertEqual(tsmp.__dict__, smp.__dict__)

    def test_update(self):
        smp = Sample2()
        with pytest.raises(Exception) as excinfo:
            smp.set_attribute_by_name("d", "8")
        assert smp.__class__.__name__ == excinfo.value.args[0]
        assert "d" == excinfo.value.args[1]

        smp = Sample2()
        smp.set_attribute_by_name("c", "8")
        assert smp.c == "8"

    def test_representation(self):
        ts1 = Sample(1)
        ts2 = Sample(2)
        ts3 = Sample(3)
        slist = [ts1, ts2, ts3]
        assert str(slist) == "[Sample({'a': 1, 'b': 3, 'c': None}), " \
                             "Sample({'a': 2, 'b': 3, 'c': None}), " \
                             "Sample({'a': 3, 'b': 3, 'c': None})]"

    def test_chain(self):
        tjson=Sample(1).to_json_compressed()
        assert isinstance(Sample(1).from_json(tjson), Sample)
        assert isinstance(Sample(1).from_dict({}), Sample)


if __name__ == '__main__':
    unittest.main()
