init:
	pip install -r requirements.txt

tests:
	coverage run --source . -m pytest -W once::DeprecationWarning
	coverage report --show-missing

lint:
	pylint simplejsonobject.py

release:
	rm -rf dist/*
	rm -rf simplejsonobject.egg-info
	python3 setup.py sdist
	twine check dist/*
	twine upload dist/*

.PHONY: init tests lint release